const express = require('express');
const router = express.Router();
const Movie = require("../models/movie.js");

const movie = new Movie();

router.post('/', function (req, res, next) {
  movie.addMovie({
    "id": "0", "title": req.body.title, "genres": req.body.genres.split(","),
    "duration": req.body.duration, "studio": req.body.studio
  });

  res.status(200).json({
    message: "sukses"
  });
});

/* GET movies listing. */
router.get('/', function (req, res, next) {
  res.status(200).json({
    movies: JSON.stringify(
      Object.fromEntries(movie.getMovies()))
  });
});

router.get('/:id', function (req, res, next) {
  res.status(200).json({
    movie: movie.getMovies(req.params.id)
  });
});

router.put('/:id', function (req, res, next) {
  movie.updateMovie(req.params.id, {
    "id": req.params.id, "title": req.body.title, "genres": req.body.genres.split(","),
    "duration": req.body.duration, "studio": req.body.studio
  });

  res.status(200).json({
    message: "sukses"
  });
});

router.delete('/:id', function (req, res, next) {
  movie.deleteMovie(req.params.id);

  res.status(200).json({
    message: "sukses"
  });
});

module.exports = router;
