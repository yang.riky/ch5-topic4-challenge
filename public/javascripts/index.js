let url = 'http://localhost:3000/api/v1/movies/';

let defaulMovie = {
    "title": "",
    "genres": "",
    "duration": 0,
    "studio": ""
};

let exampleModal = document.getElementById('exampleModal');
let modalBodyInputTitle = exampleModal.querySelector('.modal-body #floatingInputTitle');
let modalBodyTextarea = exampleModal.querySelector('.modal-body #floatingTextarea');
let modalBodyInputDuration = exampleModal.querySelector('.modal-body #floatingInputDuration');
let modalBodyInputStudio = exampleModal.querySelector('.modal-body #floatingInputStudio');

let id = 0;

function setInputValue(movie) {
    modalBodyInputTitle.value = movie.title;
    modalBodyTextarea.value = movie.genres.toString();
    modalBodyInputDuration.value = movie.duration;
    modalBodyInputStudio.value = movie.studio;
}

function getMoviesInnerHtml(movie, i) {
    return `            
            <tr id="${movie.id}">
              <th scope="row">
                ${i}
              </th>
              <td>
                ${movie.title}
              </td>
              <td>
                ${movie.genres}
              </td>
              <td>
                ${movie.duration}
              </td>
              <td>
                ${movie.studio}
              </td>
              <td>
                <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal" 
                data-bs-whatever="Edit" data-bs-id=${movie.id}><i class="bi-pencil"></i></button>
                <a class="btn btn-danger" onclick=deleteMovie(${movie.id}) role="button"><i
                    class="bi-trash"></i></a>
              </td>
            </tr>
    `;
}

function saveMovie() {
    let urlSave = url;
    let method = "";

    if (id === 0) {
        method = "post";
    } else {
        urlSave += id;
        method = "put";
    }

    fetch(urlSave, {
        method: method,
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify({
            title: modalBodyInputTitle.value,
            genres: modalBodyTextarea.value,
            duration: modalBodyInputDuration.value,
            studio: modalBodyInputStudio.value
        })
    })
        .then(response => response.json())
        .then(data => {
            showMovies();
            setInputValue(defaulMovie);
        })
        .catch(err => console.log(err));
}

function deleteMovie(id) {
    fetch(url + id, { method: 'delete' })
        .then(response => response.json())
        .then(data => {
            // document.getElementById(id).remove();
            showMovies();
        })
        .catch(err => console.log(err));
}

function showMovies() {
    fetch(url)
        .then(response => response.json())
        .then(data => {
            let movies = new Map(Object.entries(JSON.parse(data.movies)));

            let moviesInnerHtml = "";
            let i = 0;

            movies.forEach(movie => {
                moviesInnerHtml += getMoviesInnerHtml(movie, ++i);
            });

            document.getElementById("movies").innerHTML = moviesInnerHtml;

        }).catch(err => console.log(err));
}

showMovies();

exampleModal.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    let button = event.relatedTarget;

    // Extract info from data-bs-* attributes
    let action = button.getAttribute('data-bs-whatever');

    let modalTitle = exampleModal.querySelector('.modal-title');
    modalTitle.textContent = action + " Movie";

    // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    if (action === "Edit") {
        id = button.getAttribute('data-bs-id');
        fetch(url + id)
            .then(response => response.json())
            .then(data => {
                setInputValue(data.movie);
            }).catch(err => console.log(err));
    } else {
        id = 0;
        setInputValue(defaultMovie);
    }
});